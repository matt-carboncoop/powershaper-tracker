import pandas as pd


def drop_null_endpoints(df):
    while df.index[0].minute != 00:
        df = df.iloc[1:, :]
    while df.index[-1].minute != 00:
        df = df.iloc[:-1, :]
    return df


def trim(*args, tz="UTC"):
    """A helper function which trims a given number of time series dataframes so that they all correspond to the same
    time periods. Typically used to ensure that both gas, electricity, and temperature datasets cover the same time
    period. Trim undertakes the following steps:

       - copies dataframes
       - sets indexes to datetimes if not already
       - localises index to UTC (default - if other timezone applies this should be specified)
       - sorts in ascending order against DateTimeIndex
       - drops nulls at both start and end of df
       - trims the dataframes by equalising all min(df.index) and max(df.index)

       Trim requires both input dataframes to have some degree of overlap beforehand.

     Parameters
    ----------
    *args : : two or more 'pandas.DataFrame's
        A set of regular time series dataset. If index is not DateTimeIndex, function will convert accordingly. There
        must be overlap between all datasets otherwise trim will return IndexError.
    tz : : any valid timezone 'str'
        The timezone associated with the given dataframes. If timezone-naive, function will localise to 'UTC' as
        default.

     Returns
    -------
     out_df_list : :any:`list` of 'pandas.DataFrame's.
         A list of dataframes trimmed to equal total intervals, arranged in eemeter format
         (i.e. with ascending indices).
    """

    list_of_dfs = [*args]
    new_list_of_dfs = []
    for df in list_of_dfs:
        df.copy()
        if not isinstance(df.index, pd.DatetimeIndex):
            df.index = pd.to_datetime(df.index)
        if df.index.tz is None:
            df.index = df.index.tz_localize(tz=tz)  # defaults to UTC
        df = df.sort_index()
        df = drop_null_endpoints(df)
        new_list_of_dfs.append(df)
    max_start = max([min(df.index) for df in new_list_of_dfs])
    min_end = min([max(df.index) for df in new_list_of_dfs])
    out_df_list = []
    if max_start < min_end:
        for df in new_list_of_dfs:
            out_df_list.append(df.loc[max_start:min_end])
    else:
        raise IndexError("Trim requires for all dfs to have some overlap.")

    return out_df_list


# NOTE you will need to re-reference webapp_df to the webapp CSV outputted and saved on YOUR system
webapp_df = pd.read_csv(r"", index_col=0)

# NOTE you will need to re-reference this to the sample file on YOUR system
reference_df = pd.read_csv(r"", index_col=0)

webapp_df, reference_df = trim(webapp_df, reference_df)

diff = (webapp_df["Counterfactual Usage"] - reference_df["counterfactual_usage"]).sum()

print(diff)

if diff == 0:
    print("caltrack-uk-webapp correct!")
else:
    print("further tweaking required")
