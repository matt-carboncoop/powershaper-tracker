# Generated by Django 4.0.5 on 2023-02-15 12:25
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("datasets", "0002_postcode_2020_data"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="LSOAGasElec2018",
            new_name="LSOAGasElec2021",
        ),
    ]
