import importlib

from django.contrib import admin
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import path
from django.urls import reverse

from . import models
from .locator import DATASETS


class ImportableDatasetAdmin(admin.ModelAdmin):
    change_list_template = "datasets/admin_change_list.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path("import/", self.import_dataset, name="import-dataset"),
        ]
        return my_urls + urls

    def import_dataset(self, request):
        model_name = self.model._meta.model_name

        if model_name in DATASETS:
            module = importlib.import_module(f"webapp.datasets.sets.{model_name}")
            if hasattr(module, "load"):
                module.load()
                messages.success(request, "Dataset import completed")
                return redirect(reverse(f"admin:datasets_{model_name}_changelist"))

        messages.error(request, f"Dataset {model_name} importer not found.")
        return redirect(reverse(f"admin:datasets_{model_name}_changelist"))


@admin.register(models.LSOAGasElec2021)
class LSOAGasElec2021Admin(ImportableDatasetAdmin):
    pass


@admin.register(models.PostcodeGasElec2020)
class PostcodeGasElec2020Admin(ImportableDatasetAdmin):
    pass


@admin.register(models.PostcodeGasElec2021)
class PostcodeGasElec2021Admin(ImportableDatasetAdmin):
    pass
