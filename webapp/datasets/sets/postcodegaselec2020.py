import csv
from decimal import Decimal
from io import StringIO

import requests

from ..models import PostcodeGasElec2020


def load():  # pragma: no cover
    PostcodeGasElec2020.objects.all().delete()

    elec_csv = requests.get(
        "https://assets.publishing.service.gov.uk/government/uploads/system/"
        "uploads/attachment_data/file/1131257/"
        "Postcode_level_all_meters_electricity_2020.csv"
    )
    reader = csv.DictReader(StringIO(elec_csv.content.decode("us-ascii")))
    postcode_elec_data = {
        row["Postcode"]: Decimal(row["Mean_cons_kwh"]) for row in reader
    }

    gas_csv = requests.get(
        "https://assets.publishing.service.gov.uk/government/uploads/system/"
        "uploads/attachment_data/file/1131391/Postcode_level_gas_2020.csv"
    )
    reader = csv.DictReader(StringIO(gas_csv.content.decode("us-ascii")))
    postcode_gas_data = {
        row["Postcode"]: Decimal(row["Mean_cons_kwh"])
        for row in reader
        if row["Postcode"] != "All postcodes"
    }

    all_postcodes = set(postcode_elec_data.keys()) | set(postcode_gas_data.keys())

    PostcodeGasElec2020.objects.bulk_create(
        [
            PostcodeGasElec2020(
                postcode=postcode,
                elec_mean=postcode_elec_data.get(postcode, None),
                gas_mean=postcode_gas_data.get(postcode, None),
            )
            for postcode in all_postcodes
        ],
        batch_size=10000,
    )


def get(postcode: str):
    try:
        return PostcodeGasElec2020.objects.get(postcode=postcode)
    except PostcodeGasElec2020.DoesNotExist:
        raise ValueError("Couldn't find entry for this Postcode")
