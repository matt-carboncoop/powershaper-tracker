"""
Dataset module.

This is a Django app that just holds datasets.  It holds no logic apart from the logic
to import.

It was created because it's good to keep around the logic for how to load datasets from
remote sources and not just include them in the repo as it makes it easier to update
in the future.

Each dataset should have a module in the `sets` directory.  It should have:

* A function called `load`, which deletes the existing contents of the dataset and
  reloads it

* Model(s) that hold the dataset.  These should be named the same as the files except
  using Python SnakeCase instead of snake_case.

When you add a new model, you'll want to add an admin handler (if you want to view it)
and you will also want to import the model(s) in models.py.

Hopefully this wasn't a huge waste of time!
"""
