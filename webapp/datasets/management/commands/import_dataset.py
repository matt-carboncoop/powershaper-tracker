import importlib

from django.core.management.base import LabelCommand

from ...locator import DATASETS


class Command(LabelCommand):
    # See https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/

    help = "Import a dataset"
    label = "dataset"
    requires_system_checks = []
    missing_args_message = "provide a space-separated list of datasets, or use 'import_dataset list' to list datasets"

    def handle_label(self, label, *args, **options):
        if label == "list":
            print("Available datasets:")
            for name in DATASETS:
                print(f" - {name}")

        elif label in DATASETS:
            print(f"Importing {label}...")

            module = importlib.import_module(f"webapp.datasets.sets.{label}")
            if hasattr(module, "load"):
                module.load()
            else:
                print("Module has no importable data.")
            print("Done.")

        else:
            print(f"Dataset {label} doesn't exist.")
