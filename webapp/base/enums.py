from django.db import models


class MeterType(models.TextChoices):
    GAS = "GAS", "gas"
    ELEC = "ELEC", "electricity"


class PortfolioMode(models.TextChoices):
    SINGLE = "SINGLE", "a single property"
    PORTFOLIO = "PORTFOLIO", "a portfolio"


class DataOrigin(models.TextChoices):
    CSV = "CSV", "uploaded CSV files"
    PS_MONITOR = "PSM", "PowerShaper Monitor data"


class ModelType(models.TextChoices):
    HOURLY = "HOURLY", "hourly"
    DAILY = "DAILY", "daily"
