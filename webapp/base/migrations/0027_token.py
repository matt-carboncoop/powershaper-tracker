# Generated by Django 4.0.5 on 2022-12-23 15:15
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0026_caltrackdata_gas_metered_savings_hourly_monthly"),
    ]

    operations = [
        migrations.CreateModel(
            name="token",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("powershaper_token", models.CharField(blank=True, max_length=30)),
            ],
        ),
    ]
