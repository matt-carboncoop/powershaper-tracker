from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("about/", views.about, name="about"),
    path("faq/", views.faq, name="faq"),
    path("settings/", views.Settings.as_view(), name="settings"),
    path("history/", views.history, name="history"),
    path("result/<int:pk>/", views.result, name="result"),
    path(
        "download-csv/<int:pk>/", views.download_summed_csv, name="download_summed_csv"
    ),
    path(
        "download-csv-daily/<int:pk>/",
        views.download_summed_csv_daily,
        name="download_summed_csv_daily",
    ),
]
