from django import forms

from webapp.base import services


class TokenForm(forms.Form):
    token = forms.CharField(
        label="PowerShaper API token",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    def clean(self):
        data = super().clean()

        response = services.get_meter_lists(token=data["token"])
        if response.status_code != 200:
            self.add_error(
                "token",
                forms.ValidationError(
                    "Error when contacting PowerShaper API -"
                    " please check token is correct."
                ),
            )

        return data
