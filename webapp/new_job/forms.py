import csv
import datetime
import io

from dateutil.parser import isoparse
from dateutil.parser import ParserError
from dateutil.relativedelta import relativedelta
from django import forms

import webapp.base.enums as base_enums
import webapp.base.models as base_models
from webapp.base import normalise_postcode
from webapp.base import validate_household_postcode


class NewJob(forms.Form):
    portfolio_mode = forms.ChoiceField(
        label="Do you want to analyse an individual set of a results or a portfolio?",
        choices=base_enums.PortfolioMode.choices,
        widget=forms.RadioSelect(),
    )

    data_origin = forms.ChoiceField(
        label="Where do you want to source the data from?",
        choices=base_enums.DataOrigin.choices,
        widget=forms.RadioSelect(),
    )

    analyse_gas = forms.BooleanField(
        label="Analyse gas data?", initial=True, required=False
    )
    analyse_elec = forms.BooleanField(
        label="Analyse electricity data?", initial=True, required=False
    )


class UploadCSVs(forms.Form):
    gas_files = forms.FileField(required=True)
    elec_files = forms.FileField(required=True)

    # TODO(anna): depending on whether analyse_gas & analyse_elec are selected, we
    # sould present different fields

    def __init__(self, *args, **kwargs):
        analyse_gas = kwargs.pop("analyse_gas")
        analyse_elec = kwargs.pop("analyse_elec")
        self.multiple = kwargs.pop("multiple")

        super().__init__(*args, **kwargs)
        self.fields["elec_files"].label = "Electricity files"

        if self.multiple:
            self.fields["gas_files"].widget = forms.ClearableFileInput(
                attrs={"multiple": True}
            )
            self.fields["elec_files"].widget = forms.ClearableFileInput(
                attrs={"multiple": True}
            )

        if not analyse_gas:
            del self.fields["gas_files"]

        if not analyse_elec:
            del self.fields["elec_files"]

    def clean_gas_files(self):
        data = super().clean()

        if self.multiple:
            files = self.files.getlist("gas_files")
            if len(files) <= 1:
                raise forms.ValidationError(
                    "there should at least be two or more gas files uploaded"
                )
        else:
            files = [data["gas_files"]]

        parsed = [self._parse_csv(file) for file in files]
        if not self.errors:
            self.gas_data = parsed

        return data

    def clean_elec_files(self):
        data = super().clean()

        if self.multiple:
            files = self.files.getlist("elec_files")
            if len(files) <= 1:
                raise forms.ValidationError(
                    "there should at least be two or more electricity files uploaded"
                )
        else:
            files = [data["elec_files"]]

        parsed = [self._parse_csv(file) for file in files]
        if not self.errors:
            self.elec_data = parsed

        return data

    def clean(self):
        data = super().clean()

        if hasattr(self, "elec_data") and hasattr(self, "gas_data"):
            if len(self.elec_data) != len(self.gas_data):
                raise forms.ValidationError(
                    "there should be the same amount of gas and electricity files uploaded"
                )
        return data

    def _parse_csv(self, file) -> base_models.MeterData | None:
        """Parse a CSV file into an incomplete MeterData record."""
        text_wrapper = io.TextIOWrapper(file.file, encoding="utf-8")
        reader = csv.reader(text_wrapper)

        headers = None
        result = []
        for row in reader:
            if headers is None:
                headers = row
                if headers[0].lower() not in ["datetime", "timestamp", "index"]:
                    raise forms.ValidationError(
                        f"Header of first column in {file.name} is '{headers[0]}' but"
                        " should be 'datetime', 'timestamp', 'index' (case insensitive)"
                    )
                if headers[1].lower() != "value":
                    raise forms.ValidationError(
                        f"Header of first column in {file.name} is not 'value'"
                        " (case insensitive)"
                    )
            else:
                # The first one should be an ISO timestamp
                try:
                    timestamp = isoparse(row[0])
                # This should just be ParserError but if there's an unexpected failure
                # within the parser then we can get a ValueError
                # https://github.com/dateutil/dateutil/issues/1265
                except (ParserError, ValueError):
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(
                            f"Line {reader.line_num} of {file.name} contains '{row[0]}'"
                            " as the timestamp, which is not a valid timestamp"
                        ),
                    )

                # The second one should be a number
                try:
                    value = float(row[1])
                except ValueError:
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(
                            f"Line {reader.line_num} of {file.name} contains '{row[1]}'"
                            " as the value, which is not a number"
                        ),
                    )

                if not self.errors:
                    result.append((timestamp, value))

        if not self.errors:
            result.sort()
            earliest_timestamp = result[0][0]
            latest_timestamp = result[-1][0]

            return base_models.MeterData(
                earliest=earliest_timestamp,
                latest=latest_timestamp,
                data=[(timestamp.isoformat(), value) for timestamp, value in result],
            )
        else:
            return None


class Finalise(forms.Form):
    postcode = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control"}))
    reference = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    floor_area = forms.FloatField(
        label="Floor area in m²",
        help_text=(
            "You may be able to find a floor area figure by "
            "<a target='_blank' href='https://www.gov.uk/find-energy-certificate'>"
            "looking at the EPC database</a>."
        ),
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    intervention_start_date = forms.DateField(
        label="Intervention start date:",
        widget=forms.DateInput(
            format=("%Y-%m-%d"),
            attrs={
                "class": "form-control",
                "type": "date",
            },
        ),
    )
    intervention_end_date = forms.DateField(
        label="Intervention end date:",
        widget=forms.DateInput(
            format=("%Y-%m-%d"),
            attrs={
                "class": "form-control",
                "type": "date",
            },
        ),
    )

    def __init__(self, *args, **kwargs):
        date_range = kwargs.pop("date_range")

        super().__init__(*args, **kwargs)

        self.selectable_start_range = (
            date_range[0] + relativedelta(days=364),
            date_range[1] - relativedelta(days=7),
        )
        self.fields["intervention_start_date"].help_text = (
            f"Select a date between "
            f" {self.selectable_start_range[0].strftime('%d/%m/%Y')} and"
            f" {self.selectable_start_range[1].strftime('%d/%m/%Y')}"
        )

        self.latest_meter_date = date_range[1]
        self.latest_weather_date = datetime.date.today() - datetime.timedelta(days=6)
        self.latest_date = min(self.latest_meter_date, self.latest_weather_date)
        self.fields["intervention_end_date"].help_text = (
            "Select a date between the start date and"
            f" {self.latest_date.strftime('%d/%m/%Y')}"
        )

    def clean_postcode(self):
        postcode = self.cleaned_data["postcode"]
        postcode = normalise_postcode(postcode)
        if not validate_household_postcode(postcode):
            self.add_error(
                "postcode",
                forms.ValidationError("This is not a valid UK domestic postcode"),
            )

        return postcode

    def clean_intervention_start_date(self):
        start_date = self.cleaned_data["intervention_start_date"]

        if start_date < self.selectable_start_range[0]:
            # Because there is no nice way to say "please choose this date or a later
            # one", so we ask for after the day before the start of the range.
            date_after = self.selectable_start_range[0] - relativedelta(days=1)
            self.add_error(
                "intervention_start_date",
                forms.ValidationError(
                    "This date is too early because meter data does not go back this"
                    " far. Select a date after"
                    f" {date_after.strftime('%d/%m/%Y')}."
                ),
            )

        if start_date > self.selectable_start_range[1]:
            self.add_error(
                "intervention_start_date",
                forms.ValidationError(
                    "This date is too late because 365 days of data are required before"
                    " the intervention period. Select a date before "
                    f" {self.selectable_start_range[1].strftime('%d/%m/%Y')}."
                ),
            )

        return start_date

    def clean_intervention_end_date(self):
        end_date = self.cleaned_data["intervention_end_date"]

        if end_date > self.latest_meter_date:
            self.add_error(
                "intervention_end_date",
                forms.ValidationError(
                    f"This date is too late because the meter data ends at"
                    f" {self.latest_date.strftime('%d/%m/%Y')}. Select an"
                    f" earlier date."
                ),
            )
        elif end_date > self.latest_weather_date:
            self.add_error(
                "intervention_end_date",
                forms.ValidationError(
                    f"This date is too late because temperature data is only available"
                    f" a week in arrears. Select a date before "
                    f" ({self.latest_date.strftime('%d/%m/%Y')})."
                ),
            )

        return end_date

    def clean(self):
        data = super().clean()

        if data["intervention_end_date"] < data["intervention_start_date"]:
            self.add_error(
                "intervention_end_date",
                forms.ValidationError("The end date must be after the start date."),
            )

        return data
