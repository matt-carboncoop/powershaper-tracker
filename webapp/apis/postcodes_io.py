import requests

POSTCODES_IO_URL = "https://api.postcodes.io/postcodes/"


def get_lsoa_for_postcode(postcode: str) -> str:
    """
    Get the LSOA for a given postcode, using the postcodes.io lookup.

    The API result is documented here: https://postcodes.io/
    """
    response = requests.get(f"{POSTCODES_IO_URL}{postcode}", timeout=5)

    if response.status_code == 404:
        raise LookupError("Postcode not recognised")
    elif response.status_code != 200:
        raise LookupError(f"postcodes.io API returned code {response.status_code}")

    try:
        result = response.json()
        return result["result"]["codes"]["lsoa"]
    except Exception:
        raise LookupError("Postcode not recognised")
