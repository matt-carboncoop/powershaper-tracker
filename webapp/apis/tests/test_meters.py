import datetime
import os

import pytest
import requests_mock

from .. import meters


def test_datetime_aware_load():
    result = meters._loader("2020-01-03T03:34:43Z", datetime.datetime)
    assert result.year == 2020
    assert result.month == 1
    assert result.day == 3
    assert result.hour == 3
    assert result.minute == 34


@pytest.mark.skip("Requires a working token")
def test_get_meterconsent_list_can_parse_live_server_results():
    result = meters.get_meterconsent_list(token=os.getenv("POWERSHAPER_TOKEN"))
    assert result is not None


def test_get_meterconsent_info_works():
    with requests_mock.Mocker() as mocker:
        mocker.get(
            "https://app.powershaper.io/meters/api/v1/meters/?meter=40cd59ad-9ccb-4cbe-bb5e-8b0effd710dd",
            json=[
                {
                    "consent_last_four_digits": "1205",
                    "consent_name": "freds",
                    "consent_uuid": "40cd59ad-9ccb-4cbe-bb5e-8b0effd710dd",
                    "links": {
                        "records": "/meters/api/v1/meter/40cd59ad-9ccb-4cbe-bb5e-8b0effd710dd/electricity"
                    },
                    "range": {
                        "earliest": "2019-05-10T02:00:00",
                        "latest": "2023-02-23T03:00:00",
                    },
                    "status": "active",
                    "type": "electricity",
                    "postcode": "M1 1HR",
                },
                {
                    "consent_last_four_digits": "1205",
                    "consent_name": "freds",
                    "consent_uuid": "40cd59ad-9ccb-4cbe-bb5e-8b0effd710dd",
                    "links": {
                        "records": "/meters/api/v1/meter/40cd59ad-9ccb-4cbe-bb5e-8b0effd710dd/gas"
                    },
                    "range": {
                        "earliest": "2019-06-10T03:00:00",
                        "latest": "2023-02-23T03:00:00",
                    },
                    "status": "active",
                    "type": "gas",
                    "postcode": "M1 1HR",
                },
            ],
        )
        result = meters.get_meterconsent_info(
            token="", uuid="40cd59ad-9ccb-4cbe-bb5e-8b0effd710dd"
        )

    assert result is not None


@pytest.mark.skip("Requires a working token & meter uuid")
def test_get_meter_records_can_parse_live_server_results():
    result = meters.get_meter_records(
        token=os.getenv("POWERSHAPER_TOKEN"),
        meter_uuid=os.getenv("POWERSHAPER_UUID"),
        type="electricity",
        start=datetime.date(2021, 1, 1),
        end=datetime.date(2021, 5, 31),
    )
    assert result is not None
