Getting started
===============

This repo requires Python 3.10 at a minimum.

To get going, make a new virtual environment and then use pip to install the
requirements file for local development:

    $ pip install -r requirements/local.txt

Then use pre-commit to install its git hooks:

    $ pre-commit install

pre-commit runs QA checks on every commit, and is run on both every local commit and
every merge requst on GitLab.

Now you'll want to make yourself a .env file based on .example.env:

    $ cp .env.example .env

.env.example is set up to use a docker postgresql instance which will be brought up automatically if you use 'make server' to start the local server.

On macOS, you'll need to install the eccodes library:

    $ brew install eccodes

You'll also need to run:

    $ python manage.py createcachetable
    $ python manage.py migrate
    $ python manage.py import_dataset lsoagaselec2021
    $ python manage.py import_dataset postcodegaselec2020   or    $ python manage.py import_dataset postcodegaselec2021

(You have to import these datasets using the CLI. The admin importing is broken and Anna couldn't figure out why.)
