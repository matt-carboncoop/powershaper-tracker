Contributing
===============

This project
PowerShaper Tracker is a web-based tool created by Carbon Co-op that allows in-depth visualisation of metered energy savings over a period of time. It is beneficial for users who have done a retrofit or are planning to do a retrofit evaluation for their homes.
To get started on this project the link provided can be used: https://gitlab.com/carboncoop/powershaper-tracker/-/blob/main/docs/getting-started.rst.

Types of Contributions

Report Bugs
Report bugs at https://gitlab.com/carboncoop/powershaper-tracker/-/issues.
If you are reporting a bug, please include:

Any details about your local setup that might be helpful in troubleshooting.
Detailed steps to reproduce the bug.


Fix Bugs
Look through the GitLab issues for bugs.

Implement Features
Look through the GitLab issues for features.

Write Documentation
PowerShaper Tracker could always use more documentation, whether as part of the
official pyoadr-vtn docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
The best way to send feedback is to file an issue at https://gitlab.com/carboncoop/powershaper-tracker/issues.

If you are proposing a feature:
-Explain in detail how it would work.
-Keep the scope as narrow as possible, to make it easier to implement.
