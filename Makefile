# Self-documenting makefile
# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html

.PHONY: help
help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# ----------------------------------------------------------------------------

.PHONY: server
server: docker-local-up ## Bring docker up and run the local server
	python -Wd manage.py runserver 8000

.PHONY: worker
worker: docker-local-up
	# this is a hack required on macOS
	OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES python -Wd manage.py rqworker

.PHONY: pip-compile
pip-compile:  ## Recompile requirements file after a change
	pip-compile -q --output-file=requirements/production.txt requirements/production.in
	pip-compile -q --output-file=requirements/local.txt requirements/local.in
	git diff requirements/

.PHONY: pip-upgrade
pip-upgrade:  ## Compile new requirements files with the latest pkg (make pip-upgrade pkg=...)
	pip-compile -qP $(pkg) --output-file=requirements/production.txt requirements/production.in
	pip-compile -qP $(pkg) --output-file=requirements/local.txt requirements/local.in
	git diff requirements/

.PHONY: pip-upgrade-all
pip-upgrade-all:  ## Compile new requirements files with latest possible versions of everything (be careful!)
	pip-compile -qU --output-file=requirements/production.txt requirements/production.in
	pip-compile -qU --output-file=requirements/local.txt requirements/local.in
	git diff requirements/

.PHONY: sync
sync:  ## Install dependencies
	pip-sync requirements/local.txt

.PHONY: docker-local-up
docker-local-up:  ## Bring up our local docker containers
	docker-compose -p caltrackukwebapp -f docker-compose/local.yml up --detach

.PHONY: docker-local-down
docker-local-down:  ## Shut down our local docker containers
	docker-compose -p caltrackukwebapp -f docker-compose/local.yml stop

.PHONY: docker-local-clean
docker-local-clean:  ## Clean system volumes (helpful for resetting broken databases)
	docker-compose -p caltrackukwebapp -f docker-compose/local.yml rm
	docker system prune --volumes -f

.PHONY: docker-build
docker-build:  ## Build the service image
	# If running in CI we have already built the image in the build stage
	if [ "${CI}" != "true" ]; then \
		docker build --tag $${SERVICE_IMAGE_TAG:-caltrackukwebapp:latest} . ; \
	fi

.PHONY: test-container
test-container: docker-build  ## Run tests of the built service docker image in a docker-compose environment
	docker-compose -p caltrackukwebapp-testing -f docker-compose/testing.yml build
	cd test-container && docker build --tag test-container:latest .
	docker-compose -p caltrackukwebapp-testing -f docker-compose/testing.yml up -d
	docker run --rm -i \
		--env BASE_URL='http://service:5000/' \
		--env USERNAME='test-superuser' \
		--env PASSWORD='test-superuser-password' \
		--network caltrackukwebapp-testing_caltrackukwebapp-network \
		test-container:latest
	docker-compose -p caltrackukwebapp-testing -f docker-compose/testing.yml down
	docker-compose -p caltrackukwebapp-testing -f docker-compose/testing.yml rm
